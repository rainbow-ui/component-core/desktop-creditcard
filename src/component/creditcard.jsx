import { Component, UISmartPanelGrid, UIText, UISelect, UINumber, CodeTable, UILabel, UIMessageHelper } from "rainbowui-desktop-core";
import { Util, r18n } from "rainbow-desktop-tools"
import PropTypes from 'prop-types';
import '../css/component.css';

export default class TwCreditCard extends Component {
    constructor(props) {
        super(props)
        this.monthCodeTable = new CodeTable([
            { id: "01", text: "01" },
            { id: "02", text: "02" },
            { id: "03", text: "03" },
            { id: "04", text: "04" },
            { id: "05", text: "05" },
            { id: "06", text: "06" },
            { id: "07", text: "07" },
            { id: "08", text: "08" },
            { id: "09", text: '09' },
            { id: "10", text: "10" },
            { id: "11", text: "11" },
            { id: "12", text: "12" }
        ]);
        this.yearCodeTable = new CodeTable([])
        this.state = {
            creditCardNo: '',
            endLength: 4,
            no1: '', no2: '', no3: '', no4: '', no2new: '', no3new: '', maskRealVal: '',
            ExpiryDate: { year: '', month: '' }
        }
        this.generateYearCodeTable()
        this.id = "creditcard-" + this.generateId()
    }

    componentWillReceiveProps(nextProps) {
        let nostr = nextProps.model[nextProps.property];
        let arr = this.group(nostr, 4);
        if (arr && arr.length > 0) {
            this.setState({ no1: arr[0], no2: arr[1], no3: arr[2], no4: arr[3] });
        } else {
            this.setState({ no1: '', no2: '', no3: '', no4: '' });
        }
        let expirdate = nextProps.model['ExpiryDate'];
        if (expirdate) {
            let arrDate = expirdate.split('-');
            if (arrDate && arrDate.length > 0) {
                this.setState({ ExpiryDate: { year: arrDate[0], month: arrDate[1] } });
            }
        } else {
            this.setState({ ExpiryDate: { year: '', month: '' } });
        }
    }

    componentDidMount() {
        let nostr = this.props.model[this.props.property];
        let arr = this.group(nostr, 4);
        if (arr && arr.length > 0) {
            this.setState({ no1: arr[0], no2: arr[1], no3: arr[2], no4: arr[3], creditCardNo: nostr })
        }

        let expirdate = this.props.model['ExpiryDate'];
        if (expirdate) {
            let arrDate = expirdate.split('-');
            if (arrDate && arrDate.length > 0) {
                this.setState({ ExpiryDate: { year: arrDate[0], month: arrDate[1] } })
            }
        }

        if (this.props.io == 'out') {
            r18n.and = '';
        }
    }
    componentDidUpdate() {
        if (this.state.ExpiryDate.year == null || this.state.ExpiryDate.month == null) {
            this.props.model.ExpiryDate = '';
        }
    }
    group(ss, step) {
        let r = [];
        function doGroup(s) {
            if (!s) return;
            r.push(s.substr(0, step));
            s = s.substr(step);
            doGroup(s)
        }
        doGroup(ss);
        return r;
    }

    render() {
        let searchIcon = Util.parseBool(this.props.searchIcon) ? true : false;
        let cardLabelNoI18n = this.props.cardLabel ? false : true;
        let dateLabelNoI18n = this.props.dateLabel ? false : true;
        let monthLableNoI18n = this.props.monthLable ? false : true;
        let yearLableNoI18n = this.props.yearLable ? false : true;

        return (
            <div id="desktop-credit-card" class="creditcard-vertical">
                <UISmartPanelGrid class="desktop-credit-card-number" column="12" >
                    <UISmartPanelGrid column="12" colspan={this.props.colspan} colspan="12" >
                        <UIText id={this.id} label={this.props.cardLabel ? this.props.cardLabel : r18n.creditCard} className="creditcard-input" io={this.props.io} enabled={this.props.enabled} required={Util.parseBool(this.props.required)}
                            validationGroup={this.props.validationGroup} widthAllocation="0,12" allowChars="0123456789" maxLength="4" noI18n={cardLabelNoI18n} layout='vertical'
                            colspan="2" model={this.state} property="no1" onBlur={this.onBlurFirst.bind(this)} onKeyUp={this.onKeyUpNo.bind(this)} onChange={this.cardNoChange.bind(this)} />
                        <UIText className="creditcard-input" allowChars="0123456789" mask={Util.parseBool(this.props.isNeedMask) ? '*:(0,4)' : null} maxLength="4" layout='horizontal' label={r18n.and} io={this.props.io} noI18n="true" widthAllocation="1,11"
                            required={Util.parseBool(this.props.required)}
                            validationGroup={this.props.validationGroup}
                            enabled={this.props.enabled} colspan="2" model={this.state} property="no2"
                            onBlur={this.onBlurCardNo.bind(this)} onKeyUp={this.onKeyUpNo.bind(this)} onChange={this.cardNoChange.bind(this)} />
                        <UIText className="creditcard-input" allowChars="0123456789" mask={Util.parseBool(this.props.isNeedMask) ? '*:(0,4)' : null} maxLength="4" layout='horizontal'
                            label={r18n.and} io={this.props.io} noI18n="true" widthAllocation="1,11" required={Util.parseBool(this.props.required)}
                            validationGroup={this.props.validationGroup} enabled={this.props.enabled} colspan="2" model={this.state} property="no3"
                            onBlur={this.onBlurCardNo.bind(this)} onKeyUp={this.onKeyUpNo.bind(this)} onChange={this.cardNoChange.bind(this)} />
                        {/* {Util.parseBool(this.props.isHiddenCode) ?
                            <UIPassword className="creditcard-input" allowChars="0123456789" maxLength="4" layout='horizontal' label={r18n.and} io={this.props.io} noI18n="true" widthAllocation="1,11" required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} enabled={this.props.enabled} colspan="2" model={this.state} property="no2" onBlur={this.onBlurCardNo.bind(this)} onKeyUp={this.onKeyUpNo.bind(this)} onChange={this.cardNoChange.bind(this)} />
                            :
                            <UIText className="creditcard-input" allowChars="0123456789" maxLength="4" layout='horizontal' label={r18n.and} io={this.props.io} noI18n="true" widthAllocation="1,11" required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} enabled={this.props.enabled} colspan="2" model={this.state} property="no2" onBlur={this.onBlurCardNo.bind(this)} onKeyUp={this.onKeyUpNo.bind(this)} onChange={this.cardNoChange.bind(this)} />
                        }
                        {Util.parseBool(this.props.isHiddenCode) ?
                            <UIPassword className="creditcard-input" allowChars="0123456789" maxLength="4" layout='horizontal' label={r18n.and} io={this.props.io} noI18n="true" widthAllocation="1,11" required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} enabled={this.props.enabled} colspan="2" model={this.state} property="no3" onBlur={this.onBlurCardNo.bind(this)} onKeyUp={this.onKeyUpNo.bind(this)} onChange={this.cardNoChange.bind(this)} />
                            :
                            <UIText className="creditcard-input" allowChars="0123456789" maxLength="4" layout='horizontal' label={r18n.and} io={this.props.io} noI18n="true" widthAllocation="1,11" required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} enabled={this.props.enabled} colspan="2" model={this.state} property="no3" onBlur={this.onBlurCardNo.bind(this)} onKeyUp={this.onKeyUpNo.bind(this)} onChange={this.cardNoChange.bind(this)} />
                        } */}
                        <UIText className="creditcard-input" allowChars="0123456789" maxLength={this.state.endLength} layout='horizontal' io={this.props.io} label={r18n.and} noI18n="true" required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} widthAllocation="1,11" enabled={this.props.enabled} colspan="2" model={this.state} property="no4" onBlur={this.onBlurEnd.bind(this)} onKeyUp={this.onKeyUpNo.bind(this)} onFocus={this.onFocusEnd.bind(this)} onChange={this.cardNoChange.bind(this)} />

                        {this.props.monthCodeTableName ? <UISelect label={this.props.dateLabel ? this.props.dateLabel : r18n.effectiveDate} io={this.props.io} required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} placeholder={r18n.month} widthAllocation="5,7" enabled={this.props.enabled} noI18n={dateLabelNoI18n} codeTableName={this.props.monthCodeTableName} layout='vertical' colspan="2" model={this.state.ExpiryDate} property="month" onChange={this.monthChange.bind(this)} /> :
                            this.props.monthCodeTable ? <UISelect label={this.props.dateLabel ? this.props.dateLabel : r18n.effectiveDate} io={this.props.io} required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} placeholder={r18n.month} widthAllocation="5,7" enabled={this.props.enabled} noI18n={dateLabelNoI18n} codeTable={this.props.monthCodeTable} layout='vertical' colspan="2" model={this.state.ExpiryDate} property="month" onChange={this.monthChange.bind(this)} /> :
                                <UISelect label={this.props.dateLabel ? this.props.dateLabel : r18n.effectiveDate} noI18n={dateLabelNoI18n} io={this.props.io} required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} placeholder={r18n.month} widthAllocation="5,7" enabled={this.props.enabled} layout='vertical' colspan="2" model={this.state.ExpiryDate} property="month" codeTable={this.monthCodeTable} onChange={this.monthChange.bind(this)} />
                        }

                        {this.props.yearCodeTableName ? <UISelect codeTableName={this.props.yearCodeTableName} enabled={this.props.enabled} io={this.props.io} required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} placeholder={r18n.year} widthAllocation="1,11" layout='horizontal' colspan="2" model={this.state.ExpiryDate} property="year" onChange={this.yearOnChange.bind(this)} /> :
                            this.props.yearCodeTable ? <UISelect codeTable={this.props.yearCodeTable} enabled={this.props.enabled} io={this.props.io} required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} placeholder={r18n.year} widthAllocation="1,11" layout='horizontal' colspan="2" model={this.state.ExpiryDate} property="year" onChange={this.yearOnChange.bind(this)} /> :
                                <UISelect layout='horizontal' colspan="2" enabled={this.props.enabled} placeholder={r18n.year} io={this.props.io} required={Util.parseBool(this.props.required)} validationGroup={this.props.validationGroup} model={this.state.ExpiryDate} widthAllocation="1,11" property="year" codeTable={this.yearCodeTable} onChange={this.yearOnChange.bind(this)} />
                        }
                    </UISmartPanelGrid>
                </UISmartPanelGrid>
            </div>
        )
    }

    generateId() {
        let index = new Date().getTime();
        return "rainbow-ui-" + (index++);
    }

    yearOnChange(e) {
        let year = e.newValue;
        let month = this.state.ExpiryDate.month;
        this.setState({ ExpiryDate: { year, month } });
        let exdate = '';
        if (year && year + ''.length > 0 || month && month.length > 0) {
            exdate = year + '-' + month + '-01';
            this.props.model.ExpiryDate = exdate;
        }
        if (this.props.onChange) {
            // let exdate = '';
            // if (year && month && year + ''.length > 0 && month.length > 0) {
            //     exdate = year + '-' + month + '-01'
            // }
            this.props.onChange({ ExpiryDate: exdate, creditCardNo: this.state.creditCardNo });
        }
    }

    monthChange(e) {
        let year = this.state.ExpiryDate.year;
        let month = e.newValue;
        this.setState({ ExpiryDate: { month, year } });
        let exdate = '';
        if (year && year + ''.length > 0 || month && month.length > 0) {
            exdate = year + '-' + month + '-01';
            this.props.model.ExpiryDate = exdate;
        }
        if (this.props.onChange) {
            // let exdate = '';
            // if (year && month && year + ''.length > 0 && month.length > 0) {
            //     exdate = year + '-' + month + '-01';
            // }
            this.props.onChange({ ExpiryDate: exdate, creditCardNo: this.state.creditCardNo });
        }
    }

    onBlurCardNo() {
    }

    onFocusEnd() {
        let no1 = this.state.no1 ? this.state.no1.toString() : '';
        let preTwo = no1.substr(0, 2);
        let endLength = 4;
        if (preTwo && preTwo == "37") {
            endLength = 3;
        }
        this.setState({ endLength })
    }

    onBlurEnd() {
        let no1 = this.state.no1 ? this.state.no1.toString() : '';
        let no2 = this.state.no2 ? this.state.no2.toString() : '';
        let no3 = this.state.no3 ? this.state.no3.toString() : '';
        let no4 = this.state.no4 ? this.state.no4.toString() : '';
        let creditCardNo = no1 + no2 + no3 + no4;
        let preTwo = no1.substr(0, 2);
        if (preTwo && preTwo == "37" && creditCardNo.length != 15) {
            no4 = '';
            creditCardNo = no1 + no2 + no3 + no4;
            UIMessageHelper.error(r18n.regexp2)
        }
        if (preTwo && preTwo == "35" && creditCardNo.length != 16) {
            no4 = '';
            creditCardNo = no1 + no2 + no3 + no4;
            UIMessageHelper.error(r18n.regexp3)
        }
        let pre = no1.substr(0, 1);
        if (pre && (pre == "2" || pre == "4" || pre == "5") && creditCardNo.length != 16) {
            no4 = '';
            creditCardNo = no1 + no2 + no3 + no4;
            UIMessageHelper.error(r18n.regexp3)
        }
        // this.props.model.creditCardNo = creditCardNo;
        this.props.model[this.props.property] = creditCardNo;
        this.setState({ creditCardNo, no4 })
    }

    // cardNoChange2(ev) {
    //     // this.state.no2new = ev.newValue;
    //     if (this.props.onChange) {
    //         let exdate = '';
    //         if (this.state.ExpiryDate.year + ''.length > 0 && this.state.ExpiryDate.month.length > 0) {
    //             exdate = this.state.ExpiryDate.year + '-' + this.state.ExpiryDate.month + '-01'
    //         }
    //         if (this.state.creditCardNo.length > 4) {
    //             this.state.creditCardNo = this.state.creditCardNo.replace(this.state.creditCardNo.substr(4,4), ev.newValue);
    //         }
    //         if (this.state.creditCardNo.length == 12 && this.state.creditCardNo.substr(4,4) == '****') {
    //             this.state.creditCardNo = this.state.creditCardNo.replace(this.state.creditCardNo.substr(4,4), this.state.no3new);
    //         }
    //         if (this.state.creditCardNo.length == 16 && this.state.creditCardNo.substr(8,4) == '****') {
    //             this.state.creditCardNo = this.state.creditCardNo.replace(this.state.creditCardNo.substr(8,4), this.state.no3new);
    //         }
    //         this.props.onChange({ creditCardNo: this.state.creditCardNo, ExpiryDate: exdate })
    //     }
    // }
    // cardNoChange3(ev) {
    //     this.state.no3new = ev.newValue;
    //     if (this.props.onChange) {
    //         let exdate = '';
    //         if (this.state.ExpiryDate.year + ''.length > 0 && this.state.ExpiryDate.month.length > 0) {
    //             exdate = this.state.ExpiryDate.year + '-' + this.state.ExpiryDate.month + '-01'
    //         }
    //         if (this.state.creditCardNo.length > 8) {
    //             this.state.creditCardNo = this.state.creditCardNo.replace(this.state.creditCardNo.substr(4,4), ev.newValue);
    //             this.state.creditCardNo = this.state.creditCardNo.replace(this.state.creditCardNo.substr(8,4), this.state.no3new);
    //         }
    //         this.props.onChange({ creditCardNo: this.state.creditCardNo, ExpiryDate: exdate })
    //     }
    // }

    cardNoChange(ev) {
        if (this.props.onChange) {
            let exdate = '';
            if (this.state.ExpiryDate.year + ''.length > 0 && this.state.ExpiryDate.month.length > 0) {
                exdate = this.state.ExpiryDate.year + '-' + this.state.ExpiryDate.month + '-01'
            }
            // this.state.maskRealVal = this.state.no2new + this.state.no3new;
            // this.state.maskRealVal = this.state.creditCardNo.replace('********', this.state.maskRealVal);
            // this.state.creditCardNo = this.state.maskRealVal;
            // this.props.onChange({ creditCardNo: this.state.maskRealVal, ExpiryDate: exdate })
            let no1 = this.state.no1 ? this.state.no1.toString() : '';
            let no2 = this.state.no2 ? this.state.no2.toString() : '';
            let no3 = this.state.no3 ? this.state.no3.toString() : '';
            let no4 = this.state.no4 ? this.state.no4.toString() : '';
            let creditCardNo = no1 + no2 + no3 + no4;
            this.props.model[this.props.property] = creditCardNo;
            this.setState({ creditCardNo })
            this.props.onChange({ ExpiryDate: exdate, creditCardNo: creditCardNo })
        }
    }

    onBlurFirst() {
        let no1 = this.state.no1 ? this.state.no1.toString() : '';
        let pre = no1.substr(0, 1);
        let pre2 = no1.substr(1, 1);
        let no2 = this.state.no2 ? this.state.no2.toString() : '';
        let no3 = this.state.no3 ? this.state.no3.toString() : '';
        let no4 = this.state.no4 ? this.state.no4.toString() : '';
        if (pre && !(pre == "2" || pre == "3" || pre == "4" || pre == "5")) {
            no1 = '';
            UIMessageHelper.error(r18n.regexp)
        }
        if (pre && pre == "3" && pre2 && !(pre2 == "5" || pre2 == "7")) {
            no1 = '';
            UIMessageHelper.error(r18n.regexp1)
        }
        let preTwo = no1.substr(0, 2);
        if (preTwo && preTwo == "37" && no4.length == 4) {
            no4 = '';
        }
        if (preTwo && preTwo == "35" && no4.length == 3) {
            no4 = '';
        }
        let creditCardNo = no1 + no2 + no3 + no4;
        // this.props.model.creditCardNo = creditCardNo;
        this.props.model[this.props.property] = creditCardNo;
        this.setState({ creditCardNo, no1, no4 })
    }

    onKeyUpNo() {
        this.goNextInput('.creditcard-input');
        let no1 = this.state.no1 ? this.state.no1.toString() : '';
        let no2 = this.state.no2 ? this.state.no2.toString() : '';
        let no3 = this.state.no3 ? this.state.no3.toString() : '';
        let no4 = this.state.no4 ? this.state.no4.toString() : '';
        let creditCardNo = no1 + no2 + no3 + no4;
        // this.props.model.creditCardNo = creditCardNo;
        this.props.model[this.props.property] = creditCardNo;
        this.setState({ creditCardNo })
    }

    goNextInput(el) {
        let currId = event.target.id
        let txts = document.querySelectorAll(el);
        let currElement;
        let currIndex = 0;
        for (let i = 0; i < txts.length; i++) {
            if (txts[i].id == currId) {
                currElement = txts[i];
                currIndex = i;
            }
        }
        let t = txts[currIndex];
        t.index = currIndex;
        t.onkeyup = function () {
            if (this.value && this.value.length == 4) {
                let next = currIndex + 1;
                if (next > txts.length - 1) return;
                if (txts[next].value && txts[next].value.length === 4) return;
                txts[next].focus();
            }
        }
    }

    generateYearCodeTable() {
        let date = new Date();
        if (this.props.model && this.props.model.ExpiryDate) {
            let expiryDate = new Date(this.props.model.ExpiryDate);
            // 信用卡的截止日年份 < 当前日期的年份，则从截止日年份开始往后倒10年加载下拉框
            if (expiryDate.getFullYear() < date.getFullYear()) {
                date = expiryDate
            }
        }
        let nowYear = date.getFullYear();
        let yearList = [];
        for (let i = 0; i < 10; i++) {
            let item = { id: nowYear + i, text: ((nowYear + i) + '').substr(2, 2) };
            yearList.push(item)
        }
        // for(let i=1; i<11; i++){
        //     let item = { id: nowYear-i, text: nowYear-i };
        //     yearList.push(item)
        // }
        yearList.sort((a, b) => {
            return a.id - b.id;
        })
        this.yearCodeTable = new CodeTable(yearList)
    }

};

TwCreditCard.propTypes = $.extend({}, Component.propTypes, {
    label: PropTypes.string,
    codeTable: PropTypes.any,
    isHiddenCode: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    codeTableName: PropTypes.string,
    isNeedMask: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
});


TwCreditCard.defaultProps = $.extend({}, Component.defaultProps, {
    isHiddenCode: false,
    colspan: "2",
    isNeedMask: true
});
